import functions from './moduls/functions'
import datePicker from './libs/DatePickerX.min';
import helper from './moduls/helper';
import loadmore_media from './moduls/loadmore-media';
import matches from './moduls/matches';
import vote from './moduls/vote';






window.onload = () => {

    ////////// preloader

    functions.remove_preloader();


    ////////////////// search input
    
    functions.search();

    //////// tabs 
    functions.matches_tabs('.switch1', '.inside-tab2');
    functions.matches_tabs('.summary-box__tab', '.single-match__tab-content');
    functions.matches_tabs('.switch2', '.tab-switch2');
    functions.matches_tabs('.table-tabs__tab', '.table-switch-content');
    functions.matches_tabs('.player-stat-tab', '.player-stat');
    functions.matches_tabs('.switch3', '.inside-tab');


    functions.expand_and_collapse('.header__burger', '.header__nav', '-', 'active')
    functions.expand_and_collapse('.header__mobile-close', '.header__nav', 'active', 'active')
    functions.expand_and_collapse('.header__more-links', '.header__megamenu', 'active', 'active')
    functions.expand_and_collapse('.header__search .icon','.header__search', 'active', 'active')
    functions.expand_and_collapse('.single-story__open-share','.social-share-story', 'active', 'active')
    // functions.tabs()

    functions.select_to_redirect(`.comp-select-season`);
    
    
   
    
    ///////////// matches calender
  

  let calendar = document.getElementById('date-picker')
  if (calendar) {
    functions.expand_and_collapse('.live-btn','.matches-today', 'active', 'd-none')

    datePicker.startCalendar()
    // console.log(calendar.value)
    calendar.addEventListener('change',()=>{

      window.location = `/matches/${calendar.value}`;
  })
  
  }
  /////// sticky blocks // - check view width
    if (screen.width > 1024) {
        if ( document.getElementById('app-content').getAttribute('page-type') != "single-story") {
            functions.stickySide('.layout__item--mini', '.layout', 90)
        }
///////////// share to social 
        if (document.querySelector('.social-share')) {
            functions.stickySide('.social-share', '.single-wrapper', 105)
        
            // functions.copytext();
        }

    }else{
        
       
        // 
    }
    if (document.querySelector('.social-share') || document.querySelector('.social-share-story')) {
        functions.social_sharing();
    }
    
    functions.replaceMatches();

////////// handle img errors 
    let images = document.querySelectorAll('.post-news__img > img')
    if (images.length > 0) {
        // console.log(images)
        functions.lazy_load()
    }
///////// breaking news bottom bar 
    let break_news = document.querySelector('.break-news')
    if(break_news){
        functions.hide_break_news()
    }

    if(helper.page_type() == 'home'){
        loadmore_media.home_filter_news()
        //- vote
        vote.start_vote()
    }
    if(helper.page_type() == 'news' || helper.page_type() == 'video' || helper.page_type() == 'section' || helper.page_type() == 'tag' || helper.page_type() == 'infographic'){
        loadmore_media.load_more()
    }

if(helper.page_type() == 'matches'){
    matches.fill_teams("matches",`.matches-comps`,`.matches-teams`,`.matches-comps input`);
    // console.log('$')
}
if(helper.page_type() == 'team' ){
    console.log('$')
    matches.fill_teams("team",`.select-comp`,`.select-season`,`.select-comp input`);
    
   
}
if(helper.page_type() ==  'player'){
    console.log('$')
    matches.fill_teams("player",`.select-comp`,`.select-season`,`.select-comp input`);
    
   
}
    
    

    
    
}