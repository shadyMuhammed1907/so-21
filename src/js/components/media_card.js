import helper from "../moduls/helper"



let card = {}

card.card_theme = (data) => {
    let category_link = ''
    let category_icon = ''
    let category_title = ''
    let video_icon = ''
    let modifier = ''
    if(data.type == 'video'){
        modifier = 'post-news--video'
        video_icon = `<span class="post-news__video">
        <svg id="Iconly_Bulk_Play" data-name="Iconly/Bulk/Play" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
            <g id="Play">
                <path id="Fill_1" data-name="Fill 1" d="M50,25.012A25,25,0,1,1,25,0,25.03,25.03,0,0,1,50,25.012" fill="#d91d1d"></path>
                <path id="Fill_4" data-name="Fill 4" d="M17.5,10.012a2.882,2.882,0,0,1-.6,1.774,9.652,9.652,0,0,1-.7.778l-.149.146A37.881,37.881,0,0,1,6.14,19.32,8.687,8.687,0,0,1,3.828,20h-.1A3.136,3.136,0,0,1,.969,18.35a13.655,13.655,0,0,1-.6-2.211A44.072,44.072,0,0,1,0,9.988,38.032,38.032,0,0,1,.423,3.645,15.954,15.954,0,0,1,.87,1.871,3.189,3.189,0,0,1,2.262.365,3.555,3.555,0,0,1,3.828,0,8.607,8.607,0,0,1,5.891.559,38.06,38.06,0,0,1,16.033,7.266c.348.34.721.756.82.851a3.024,3.024,0,0,1,.646,1.9" transform="translate(17.5 15)" fill="#fff"></path>
            </g>
        </svg></span>`
    } else {
        modifier = ''
        video_icon = ''
    }
    if(data.type == 'news'){
        category_icon = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="15" viewBox="0 0 30 15">
        <defs>
            <linearGradient id="linear-gradient" x1="0.92" y1="0.5" x2="0" y2="0.5" gradientUnits="objectBoundingBox">
                <stop offset="0" stop-color="#24a557"></stop>
                <stop offset="1" stop-color="#bafb3c"></stop>
            </linearGradient>
        </defs>
        <rect id="your_logo_here" data-name="your logo here" width="30" height="15" rx="2" fill="url(#linear-gradient)"></rect>
    </svg>`
    category_link = data.sections.slug
    category_title = data.sections.title
    } else if (data.type == 'infographic'){
        category_icon = `<svg id="Iconly_Bulk_Graph" data-name="Iconly/Bulk/Graph" xmlns="http://www.w3.org/2000/svg" width="19.02" height="19.02" viewBox="0 0 19.02 19.02">
        <g id="Graph">
          <path id="Path" d="M7.753.613a1.008,1.008,0,0,1,.095.313l.265,3.937h0l.131,1.979a2.035,2.035,0,0,0,.095.6.992.992,0,0,0,.956.6l6.331-.414a1.044,1.044,0,0,1,.736.285,1.008,1.008,0,0,1,.3.579l.011.133a8.256,8.256,0,0,1-6.547,7.435A8.422,8.422,0,0,1,1.009,12,7.812,7.812,0,0,1,.061,9.013,5.723,5.723,0,0,1,0,8.075,8.269,8.269,0,0,1,6.642.013,1.038,1.038,0,0,1,7.753.613Z" transform="translate(0 2.769)" fill="#171c19"></path>
          <path id="Path-2" data-name="Path" d="M.807,0A8.981,8.981,0,0,1,9.49,7.429l-.007.031h0l-.019.045,0,.124a.761.761,0,0,1-.182.449.787.787,0,0,1-.44.258l-.1.014-7.3.473a.872.872,0,0,1-.666-.216A.84.84,0,0,1,.5,8.1L.006.8a.116.116,0,0,1,0-.076A.746.746,0,0,1,.252.2.775.775,0,0,1,.807,0Z" transform="translate(9.53)" fill="#17a654" opacity="0.4"></path>
        </g>
      </svg>`
      category_link = 'infographics'
      category_title = 'إنفوجراف'
    }
    let link = `${location.origin}/${data.slug}`
    let html = 
    `
    <div class="post-news post-news--card post-news--mobile ${modifier}">
        <a class="post-news__link" href="${link}" title="${data.title}"></a>
        <div class="post-news__img"> 
            <img  src="${helper.image_server}${data.images[0].url}" alt="${data.title}" title="${data.title}" onerror="this.src='img/no-img.png';">
            ${video_icon}
        </div>
        <div class="post-news__data">`

        if(data.type != 'video'){
            html += `<a class="post-news__category" href="${location.origin}/${category_link}" title="${data.title}">
            <span class="icon">
            ${category_icon}
            </span>
            <p class="text-gradient">
                ${category_title}
            </p>
        </a>`
        }

        html += `<p class="post-news__title">${data.title}</p>
        <div class="post-news__info">
            <div class="post-news__time">${data.published_date}</div>
            <div class="post-news__views">
                <span class="icon">
                    <svg id="Iconly_Light-outline_Show" data-name="Iconly/Light-outline/Show" xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                    <g id="Show" transform="translate(1.79 3.58)">
                        <path id="Show-2" data-name="Show" d="M8.692,14.316c-3.5-.116-6.676-2.585-8.51-6.6L.054,7.424a.665.665,0,0,1,0-.527A12.774,12.774,0,0,1,3.807,1.843,8.357,8.357,0,0,1,8.952,0l.256,0C12.7.121,15.884,2.591,17.718,6.61l.125.28a.667.667,0,0,1,0,.533,12.783,12.783,0,0,1-3.754,5.054,8.357,8.357,0,0,1-5.144,1.843ZM4.771,2.791a11.028,11.028,0,0,0-3.226,4.07l-.14.3.151.323a11.191,11.191,0,0,0,3.025,3.9,7.142,7.142,0,0,0,3.9,1.584l.238.012.23,0,.228,0a7.107,7.107,0,0,0,4.148-1.606,11.336,11.336,0,0,0,3.156-4.2l.009-.022A11.452,11.452,0,0,0,13.43,3.024,7.184,7.184,0,0,0,9.411,1.35l-.238-.012-.214,0A7.005,7.005,0,0,0,4.771,2.791Zm.678,4.37a3.5,3.5,0,1,1,3.5,3.478A3.494,3.494,0,0,1,5.449,7.161Zm1.343,0A2.158,2.158,0,1,0,8.95,5.016,2.154,2.154,0,0,0,6.792,7.161Z" transform="translate(0)" fill="#17a654"></path>
                    </g>
                    </svg>
                </span>
                <p>${data.hits}</p>
            </div>
        </div>
        </div>
    </div>
    `

    return html
}

export default card