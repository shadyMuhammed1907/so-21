import functions from './functions'

let matches = {},
    base_url = "https://newweb.so3ody.com/";
matches.fill_teams = (typeOfPage,selector1,selector2,input)=>{
	let comps_filter = document.querySelector(selector1),
		teams_filter = document.querySelector(selector2),
		comps_inputs = document.querySelectorAll(input),
		comps_input_id ,dom_str,player_id,
		rq_url = "";

		for (let i = 0; i < comps_inputs.length; i++) {
			comps_input_id = comps_inputs[i].value
			comps_inputs[i].addEventListener(`change`,()=>{
				

				
                comps_input_id = comps_inputs[i].value
				
////////// competitions filter
                
				matches.filter_comp(comps_input_id,`.round-box`,false);
				
				if (comps_input_id != "0") {
					teams_filter.classList.add(`disabled`)
					if (typeOfPage == "matches") {
						
					}
					if (typeOfPage == "matches") {
						matches.filter_reset(`.match-card-wide`,true);
						rq_url = `${base_url}api/get/competition/teams?competitionId=${comps_input_id}`
					}
					if (typeOfPage == "team" ) {
						rq_url = `${base_url}api/teams/get/comp/seasons/${comps_input_id}`
					}
					if (typeOfPage ==  "player") {
						player_id = comps_inputs[i].getAttribute('player-id')
						// rq_url = `${base_url}api/player/get/comp/seasons/${comps_input_id}`
						rq_url = `${base_url}api/player/get_seasons?compId=${comps_input_id}&playerId=${player_id}`
					}
				fetch(rq_url)
				.then(response => response.json())
				.then((res)=>{
					// console.log(res)
					dom_str = `
					<div class="select-theme__current" tabindex="2">
					<div class="select-theme__value">
					  <input class="select-theme__input" id="team0" type="radio" value="0" name="teams" checked="checked">
					  <p class="select-theme__input-text"> ${typeOfPage == "matches" ? "كل الفرق":"اختر موسم"} </p></div>

					`
					if (typeOfPage == "matches") {
						for (let i = 0; i < res.data.teams.length; i++) {
						
							dom_str +=`
							<div class="select-theme__value">
						  <input class="select-theme__input" id="team${res.data.teams[i].teamId}" type="radio" value="${res.data.teams[i].teamId}" name="teams" ">
						  <p class="select-theme__input-text">${res.data.teams[i].name}</p></div>
							` 
						}
					}
					if (typeOfPage == "team" || "player") {
						for (let i = 0; i < res.data.length; i++) {
						
							dom_str +=`
							<div class="select-theme__value">
						  <input class="select-theme__input" id="team${res.data[i].seasonId}" type="radio" value="${res.data[i].seasonId}" name="teams"  data-url="${window.location.href}?seasonId=${res.data[i].seasonId}"">
						  <p class="select-theme__input-text">${res.data[i].year}</p></div>
							` 
						}
					}
					

					dom_str += `<span class="icon select-theme__icon">
					  <svg id="Capa_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 185.344 185.344" style="enable-background:new 0 0 185.344 185.344;" xml:space="preserve">
						<g>
						  <g></g>
						  <path d="M92.672,144.373c-2.752,0-5.493-1.044-7.593-3.138L3.145,59.301c-4.194-4.199-4.194-10.992,0-15.18                                            c4.194-4.199,10.987-4.199,15.18,0l74.347,74.341l74.347-74.341c4.194-4.199,10.987-4.199,15.18,0                                            c4.194,4.194,4.194,10.981,0,15.18l-81.939,81.934C98.166,143.329,95.419,144.373,92.672,144.373z"></path>
						</g>
					  </svg></span>
				  </div>
				  <ul class="select-theme__list">
					<li>
					  <label class="select-box__option" for="team0" aria-hidden="aria-hidden">${typeOfPage == "matches" ? "كل الفرق":"اختر موسم"} </label>
					</li>`
					
					if (typeOfPage == "matches") {
						for (let i = 0; i < res.data.teams.length; i++) {
						
							dom_str +=`
							<li>
						  <label class="select-box__option" for="team${res.data.teams[i].teamId}" aria-hidden="aria-hidden">${res.data.teams[i].name}</label>
						</li>`
							
							
						}
					}
					if (typeOfPage == "team" || "player") {
						for (let i = 0; i < res.data.length; i++) {
						
							dom_str +=`
							<li>
						  <label class="select-box__option" for="team${res.data[i].seasonId}" aria-hidden="aria-hidden">${res.data[i].year}</label>
						</li>`
							
							
						}
					}
					

					dom_str += `</ul>`;
					teams_filter.innerHTML = dom_str;
					teams_filter.classList.remove(`disabled`);
					
					typeOfPage == "matches" ? matches.filter_team() : functions.select_to_redirect(`.select-season input`);


				

				})
				} else {
					
					teams_filter.innerHTML = `<div class="select-theme__current" tabindex="2">
					<div class="select-theme__value">
					  <input class="select-theme__input" id="team0" type="radio" value="0" name="teams" checked="checked">
					  <p class="select-theme__input-text">${typeOfPage == "matches" ? "كل الفرق":"اختر موسم"}</p>
					</div><span class="icon select-theme__icon">
					  <svg id="Capa_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 185.344 185.344" style="enable-background:new 0 0 185.344 185.344;" xml:space="preserve">
						<g>
						  <g></g>
						  <path d="M92.672,144.373c-2.752,0-5.493-1.044-7.593-3.138L3.145,59.301c-4.194-4.199-4.194-10.992,0-15.18                                            c4.194-4.199,10.987-4.199,15.18,0l74.347,74.341l74.347-74.341c4.194-4.199,10.987-4.199,15.18,0                                            c4.194,4.194,4.194,10.981,0,15.18l-81.939,81.934C98.166,143.329,95.419,144.373,92.672,144.373z"></path>
						</g>
					  </svg></span>
				  </div>
				  <ul class="select-theme__list">
					<li>
					  <label class="select-box__option" for="team0" aria-hidden="aria-hidden">${typeOfPage == "matches" ? "كل الفرق":"اختر موسم"}</label>
					</li>
				  </ul>`
				}

				
			}
			)
			
		}
}
matches.filter_comp = (comp_id,comp_holder,_flag)=>{
	let comp_boxes = document.querySelectorAll(comp_holder);

		if (_flag == false) {
            // document.querySelector(`.no-matches`).classList.add(`d-none`)
			for (let i = 0; i < comp_boxes.length; i++) {
                if (comp_id == "0"){
					
                    matches.filter_reset(`.round-box`,true)
                    matches.filter_reset(`.match-card-wide`,true)
                    
				}else{
                    if (comp_boxes[i].getAttribute(`data-competition-id`) == comp_id ) {
                        for (let x = 0; x < comp_boxes.length; x++) {
                            comp_boxes[x].classList.add(`d-none`)
                        }
                        comp_boxes[i].classList.remove(`d-none`)
    
                        
                    }
                    if (comp_boxes[i].getAttribute(`data-competition-id`) != comp_id ) {
                        // matches.filter_reset(`.round-box`,false)
                        
                        comp_boxes[i].classList.add(`d-none`)
                        //  document.querySelector(`.no-matches`).classList.remove(`d-none`)
                        
                    }

                }

				
			}
		} else {

			for (let i = 0; i < comp_boxes.length; i++) {
				if (comp_boxes[i].getAttribute(`data-teama-id`) == comp_id || comp_boxes[i].getAttribute(`data-teamb-id`) == comp_id) {
					for (let i = 0; i < comp_boxes.length; i++) {
						comp_boxes[i].classList.add(`d-none`)
					}
					comp_boxes[i].classList.remove(`d-none`)
				}   
                if (comp_id == "0"){
					for (let i = 0; i < comp_boxes.length; i++) {
						comp_boxes[i].classList.remove(`d-none`)
					}
				}
				
				
			}
		}
					
		
}
matches.filter_team = ()=>{
	let team_id_input = document.querySelectorAll(`.matches-teams input`);
	for (let i = 0; i < team_id_input.length; i++) {
		team_id_input[i].addEventListener(`change`,()=>{
			matches.filter_comp(team_id_input[i].value,`.match-card-wide`,true)
		})
		
	}

}
matches.filter_reset = (e , b)=>{

    /////////////////////////// handle case of the selected comp not playing .. you need to hide all
	let reset_all = document.querySelectorAll(e);
        
    if (b == true) {
        for (let i = 0; i < reset_all.length; i++) {
            reset_all[i].classList.remove('d-none');
        }
    } else{
        
        for (let i = 0; i < reset_all.length; i++) {
            reset_all[i].classList.add('d-none');
        }
    }
	
  

}
export default matches