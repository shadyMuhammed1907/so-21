import helper from "./helper";



let vote = {}

let box = document.querySelector('.survey__btns-box')
let btns = document.querySelectorAll('.survey__btn')
let vote_text = document.querySelector('.survey__text')

let submitted_answers


vote.start_vote = () => {
    if(btns.length > 0){
        for(let i=0;i<btns.length;i++){
            btns[i].addEventListener('click',()=>{
                // console.log(btns[i])
                vote.request(parseInt(vote_text.getAttribute('vote-id')),parseInt(btns[i].getAttribute('answer-id')))
            })
        }
    }
}

vote.request = (question_id,answer_id) => {
    box.innerHTML = ''
    helper.add_loader('.survey__btns-box')
    helper.post_data('addVote',{'voteId':question_id,'answerId':answer_id})
    .then(res => res.json())
    .then((data)=>{
        submitted_answers = data.answers
        box.innerHTML = ''
        let html = ''
        let _class = ''
        let __all
        let values_arr = []
        let _percentage = []
        for(let i=0;i<submitted_answers.length;i++){
            values_arr.push(submitted_answers[i].count)
        }
        __all = values_arr.reduce((a, b) => a + b, 0)
        for(let i=0;i<submitted_answers.length;i++){
            _percentage.push(submitted_answers[i].count * 100 / __all)
        }
        for(let i=0;i<submitted_answers.length;i++){
            if(_percentage[i] == Math.max(..._percentage)){
                _class = 'high'
            } else {
                _class = ''
            }
            html += `<div class="survey__graph ${_class}">
            <div class="survey__result-line flex">
              <div class="survey__text">${submitted_answers[i].text}</div>
              <div class="survey__num mr-max">${Math.round(submitted_answers[i].count * 100 / __all )}%</div>
            </div>
            <div class="survey__prog-line"> 
              <div class="survey__prog-color" style="width:${submitted_answers[i].count * 100 / __all }%;"></div>
            </div>
          </div>`
        }
        box.innerHTML = html;
    })
}










export default vote