let helper = {};

// helper._baseUrl = 'https://newweb.so3ody.com/'
helper._baseUrl = location.origin;

helper.get_data = (url) => {
    return fetch(`${helper._baseUrl}/api/${url}`)
}

helper.post_data = (url,data) => {
    const otherParam = {
        headers:{
            'content-type':'application/json; charset=UTF-8'
        },
        body:JSON.stringify(data),
        method:'POST'
    }
    return fetch(`${helper._baseUrl}/api/${url}`,otherParam)
}

helper.image_server = `https://so3ody.s3.amazonaws.com/images`

helper.add_loader = (parent) => {
    parent = document.querySelector(parent)
    parent.innerHTML += `<div class="lds-ellipsis">
    <div></div>
    <div></div>
    <div></div>
</div>`;
}

helper.remove_loader = (parent, target) => {
    let box = document.querySelector(`${parent}`)
    target = document.querySelector(`${target}`);
    if (target) {
        box.removeChild(target)
    }
}

helper.page_type = () => {
    let app = document.getElementById('app-content');
    let type = app.getAttribute('page-type');
    return type;
}

helper.section_id = () => {
    let div = document.getElementById('sectionId')
    let sec = div.getAttribute('value')
    return sec
}
helper.tag_id = () => {
    let div = document.getElementById('tagId')
    let tag = div.getAttribute('value')
    return tag
}


export default helper