import sticky from '../libs/float-sidebar.min';
import LazyLoad from "vanilla-lazyload";

let functions = {}

let checkWidth = window.matchMedia("(min-width: 1025px)");

functions.tabs = () => {
	let bindAll = () => {
		let tabs = document.querySelectorAll('[data-tab]');
		for (let i = 0; i < tabs.length; i++) {
			tabs[i].addEventListener('click', change, false);
		}
	}
	let clear = () => {
		let tabs = document.querySelectorAll('[data-tab]');
		for (let i = 0; i < tabs.length; i++) {
			tabs[i].classList.remove('active');
			let id = tabs[i].getAttribute('data-tab');
			document.getElementById(id).classList.remove('active');
		}
	}
	let change = (e) => {
		clear();
		e.target.classList.add('active');
		let id = e.currentTarget.getAttribute('data-tab');
		document.getElementById(id).classList.add('active');
	}
	bindAll();
}
functions.remove_preloader = () => {
	setTimeout(() => {
		document.querySelector('.pre-loader').classList.remove('active');
	}, 500)

}

// -- toggle active 2 elements or 1 element if founded
functions.expand_and_collapse = (el_1, el_2, class_1, class_2) => {
	el_1 = document.querySelector(el_1)
	if (el_1 && el_2) {
		el_1.onclick = () => {
			el_1.classList.toggle(class_1)
			document.querySelector(el_2).classList.toggle(class_2);

		}
	}
	else if (el_1 && !el_2) {
		el_1.onclick = () => {
			el_1.classList.toggle(class_1)


		}
	}else{
		
	}
}

//-- Sticky
functions.stickySide = (aside, box, top) => {
	let sidebar = document.querySelector(aside);
	let content = document.querySelector(box);
	if (checkWidth.matches) {
		let floatSidebar = new sticky({
			sidebar: sidebar,
			relative: content,
			topSpacing: top,
			bottomSpacing: 20
		});
	}

}
//-- toggle match tabs
functions.matches_tabs = (_tab, _content) => {
	// console.log(_tab)
	// console.log(_content)
	let tab = document.querySelectorAll(_tab);

	if (tab) {
		let content = document.querySelectorAll(_content);
		for (let i = 0; i < tab.length; i++) {
			tab[i].onclick = () => {
				for (let x = 0; x < tab.length; x++) {

					tab[x].classList.remove('active')
					content[x].classList.remove('active')
				}
				tab[i].classList.add('active')
				content[i].classList.add('active')
			}
		}
	}


}


functions.lazy_load = () => {
	var lazyLoadInstance = new LazyLoad({
		// Your custom settings go here
	});
}

functions.hide_break_news = () => {
	let div = document.querySelector('.break-news')
	let close = document.querySelector('.break-news__close')
	close.addEventListener('click', () => {
		div.style.display = 'none'
	})
}
functions.social_sharing = () => {

	
		let copyText = document.querySelector(".cp-share-input"),
			hint_message = document.querySelector(".action-message");

		copyText.addEventListener("click", () => {
			copyText.select();
			copyText.setSelectionRange(0, 99999)
			document.execCommand("copy");
			document.querySelector(".action-message").classList.remove("active") 
			hint_message.classList.add("active")
			setTimeout(()=>{
				document.querySelector(".action-message").classList.remove("active") 
			}, "2000")
		})
	

	let fb_share = document.querySelector(".fb-share"),
		twtr_share = document.querySelector(".tw-share"),
		wts_share = document.querySelector(".wt-share");


	
		fb_share.addEventListener("click", (e) => {
			e.preventDefault()
			window.open(`http://www.facebook.com/sharer/sharer.php?u=${window.location.href}`, "_blank", "width=600,height=600")

		})

	

	
		twtr_share.addEventListener("click", (e) => {
			e.preventDefault()
			window.open(`https://twitter.com/intent/tweet?text=${window.location.href}`, "_blank", "width=400,height=400")

		})
	

	
		wts_share.addEventListener("click", (e) => {
			e.preventDefault()
			window.open(`whatsapp://send?text=${window.location.href}menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600`, "_blank", "width=400,height=400")

		})
	






}
functions.search = () =>{

	let searchInput = document.querySelector('.header__search-box input'),
		submit = document.querySelector('.enter-search');
		if (searchInput) {
			searchInput.onsearch = function () {
				if (searchInput.value.length > 2) {
					
					window.location.href = `/Search?q=${searchInput.value}`;
				}
			}
		}
		if (submit) {
			submit.onclick = function () {
				if (searchInput.value.length > 2) {
					 
					window.location.href = `/Search?q=${searchInput.value}`;
				}
			}
		}
}
functions.replaceMatches = ()=>{
	let ht_content =``,
		content_holder = document.querySelector(".switch-matches-view"),
		content_replace = document.querySelector(".home-matches");
		if (content_replace) {
			ht_content = content_holder.innerHTML;
			if (screen.width < 1024 && content_replace.innerHTML ==`` ) {
				content_replace.innerHTML = ht_content;
			}
			window.addEventListener('resize', function () {
				
			if (screen.width > 1024 ) {

				if (content_replace.innerHTML !=``) {
					content_holder.innerHTML = ht_content;
					content_replace.innerHTML = ``;
				// console.log("ss")
				}else{
					content_holder.innerHTML = ht_content;
					content_replace.innerHTML = ``;
				}
				
				
			}  
			else {
				if (content_replace.innerHTML ==``){
					content_holder.innerHTML = ``;
					content_replace.innerHTML = ht_content;
					// console.log("we")
				}
				
			}
			
			})
		}
		
		

}
functions.select_to_redirect = (_input)=>{
	let radio_btn = document.querySelectorAll(_input);
		// _link = radio_btn.getAttribute(`data-url`)
		for (let i = 0; i < radio_btn.length; i++) {
			radio_btn[i].addEventListener("change",()=>{
				// e.preventDefault()
				location.href = radio_btn[i].getAttribute('data-url');
			})
			
		}
	
}



export default functions