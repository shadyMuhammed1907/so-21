import helper from "./helper";
import card from "../components/media_card";


let loadmore_media = {};

let action_type = 'append'
let page_name = ''
let page_index = 2;
let page_size = 9;
let section_id = '';
let tag_id = ''
let page_type = helper.page_type()



let btn = document.querySelector('.btn--viewall')
let box = document.querySelector('.layout__item--large .row')


loadmore_media.load_more = () => {
    if(page_name == 'home'){
        return false;
    }
    if (btn) {
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            action_type = 'append'
            let url = ''
            if (page_type == 'news') {
                //-- News page
                page_name = 'allNews'
            } else if (page_type == 'infographic') {
                //-- Infographics page
                page_name = 'infographic'
            } else if (page_type == 'section') {
                //-- Section
                page_name = 'section'
                section_id = `&sectionId=${helper.section_id()}`
            } else if (page_type == 'tag') {
                //-- Tag
                page_name = 'tag'
                tag_id = `&tagId=${helper.tag_id()}`
            } else if (page_type == 'video') {
                //-- Video
                page_name = 'video'
            }
            url = `pageName=${page_name}&limit=${page_size}&pageIndex=${page_index}${section_id}${tag_id}`
            loadmore_media.get_data(url)
        })
    }
}

loadmore_media.looper = (collection, parent, grid, action_type) => {
    console.log(action_type)
    if (collection.length > 0) {

        for (let i = 0; i < collection.length; i++) {
            parent.innerHTML += `<div class="${grid}">${card.card_theme(collection[i])}</div>`;


            // if (action_type == 'append') {
            //     parent.innerHTML += `<div class="${grid}">${card.card_theme(collection[i])}</div>`;
            // } else if (action_type == 'change') {
            //     parent.innerHTML += `<div class="${grid}">${card.card_theme(collection[i])}</div>`;
            // }
        }
    } else {
        // btn.style.display = 'none';
        // box.innerHTML += helper.no_data()
    }
}

loadmore_media.get_data = (url) => {
    if (action_type == 'change') {
        box.innerHTML = ''
    }
    helper.add_loader('.layout__item--large .row')
    helper.get_data('more-news?' + url)
        .then(res => res.json())
        .then(data => {
            helper.remove_loader('.layout__item--large .row', '.layout__item--large .row .lds-ellipsis')
            if (page_type == 'video') {
                data = data.latestVideos
            } else {
                data = data.latestNews
            }
            loadmore_media.looper(data, box, 'col-md-4 col-sm-6', action_type)
            page_index++;
        })
}


loadmore_media.home_filter_news = () => {
    let arr = ['allNews', 'news', 'infographic']
    let filters = document.querySelectorAll('.main-title__side span')
    for (let i = 0; i < filters.length; i++) {
        filters[i].addEventListener('click', () => {
            if (!filters[i].classList.contains('active')) {
                document.querySelector('.main-title__side span.active').classList.remove('active')
                filters[i].classList.add('active')
                action_type = 'change'
                loadmore_media.get_data(`pageName=${arr[i]}&limit=${6}&pageIndex=${1}`)
            }
        })
    }

}


export default loadmore_media