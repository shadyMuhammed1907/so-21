(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t="undefined"!=typeof globalThis?globalThis:t||self).LazyLoad=n()}(this,(function(){"use strict";function t(){return(t=Object.assign||function(t){for(var n=1;n<arguments.length;n++){var e=arguments[n];for(var i in e)Object.prototype.hasOwnProperty.call(e,i)&&(t[i]=e[i])}return t}).apply(this,arguments)}var n="undefined"!=typeof window,e=n&&!("onscroll"in window)||"undefined"!=typeof navigator&&/(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),i=n&&"IntersectionObserver"in window,o=n&&"classList"in document.createElement("p"),r=n&&window.devicePixelRatio>1,a={elements_selector:".lazy",container:e||n?document:null,threshold:300,thresholds:null,data_src:"src",data_srcset:"srcset",data_sizes:"sizes",data_bg:"bg",data_bg_hidpi:"bg-hidpi",data_bg_multi:"bg-multi",data_bg_multi_hidpi:"bg-multi-hidpi",data_poster:"poster",class_applied:"applied",class_loading:"loading",class_loaded:"loaded",class_error:"error",class_entered:"entered",class_exited:"exited",unobserve_completed:!0,unobserve_entered:!1,cancel_on_exit:!0,callback_enter:null,callback_exit:null,callback_applied:null,callback_loading:null,callback_loaded:null,callback_error:null,callback_finish:null,callback_cancel:null,use_native:!1},c=function(n){return t({},a,n)},s=function(t,n){var e,i="LazyLoad::Initialized",o=new t(n);try{e=new CustomEvent(i,{detail:{instance:o}})}catch(t){(e=document.createEvent("CustomEvent")).initCustomEvent(i,!1,!1,{instance:o})}window.dispatchEvent(e)},l="loading",u="loaded",d="applied",f="error",_="native",g="data-",v="ll-status",b=function(t,n){return t.getAttribute(g+n)},p=function(t){return b(t,v)},h=function(t,n){return function(t,n,e){var i="data-ll-status";null!==e?t.setAttribute(i,e):t.removeAttribute(i)}(t,0,n)},m=function(t){return h(t,null)},E=function(t){return null===p(t)},y=function(t){return p(t)===_},I=[l,u,d,f],A=function(t,n,e,i){t&&(void 0===i?void 0===e?t(n):t(n,e):t(n,e,i))},L=function(t,n){o?t.classList.add(n):t.className+=(t.className?" ":"")+n},w=function(t,n){o?t.classList.remove(n):t.className=t.className.replace(new RegExp("(^|\\s+)"+n+"(\\s+|$)")," ").replace(/^\s+/,"").replace(/\s+$/,"")},k=function(t){return t.llTempImage},O=function(t,n){if(n){var e=n._observer;e&&e.unobserve(t)}},x=function(t,n){t&&(t.loadingCount+=n)},z=function(t,n){t&&(t.toLoadCount=n)},C=function(t){for(var n,e=[],i=0;n=t.children[i];i+=1)"SOURCE"===n.tagName&&e.push(n);return e},N=function(t,n,e){e&&t.setAttribute(n,e)},M=function(t,n){t.removeAttribute(n)},R=function(t){return!!t.llOriginalAttrs},T=function(t){if(!R(t)){var n={};n.src=t.getAttribute("src"),n.srcset=t.getAttribute("srcset"),n.sizes=t.getAttribute("sizes"),t.llOriginalAttrs=n}},G=function(t){if(R(t)){var n=t.llOriginalAttrs;N(t,"src",n.src),N(t,"srcset",n.srcset),N(t,"sizes",n.sizes)}},D=function(t,n){N(t,"sizes",b(t,n.data_sizes)),N(t,"srcset",b(t,n.data_srcset)),N(t,"src",b(t,n.data_src))},V=function(t){M(t,"src"),M(t,"srcset"),M(t,"sizes")},j=function(t,n){var e=t.parentNode;e&&"PICTURE"===e.tagName&&C(e).forEach(n)},F={IMG:function(t,n){j(t,(function(t){T(t),D(t,n)})),T(t),D(t,n)},IFRAME:function(t,n){N(t,"src",b(t,n.data_src))},VIDEO:function(t,n){!function(t,e){C(t).forEach((function(t){N(t,"src",b(t,n.data_src))}))}(t),N(t,"poster",b(t,n.data_poster)),N(t,"src",b(t,n.data_src)),t.load()}},P=function(t,n){var e=F[t.tagName];e&&e(t,n)},S=function(t,n,e){x(e,1),L(t,n.class_loading),h(t,l),A(n.callback_loading,t,e)},U=["IMG","IFRAME","VIDEO"],$=function(t,n){!n||function(t){return t.loadingCount>0}(n)||function(t){return t.toLoadCount>0}(n)||A(t.callback_finish,n)},q=function(t,n,e){t.addEventListener(n,e),t.llEvLisnrs[n]=e},H=function(t,n,e){t.removeEventListener(n,e)},B=function(t){return!!t.llEvLisnrs},J=function(t){if(B(t)){var n=t.llEvLisnrs;for(var e in n){var i=n[e];H(t,e,i)}delete t.llEvLisnrs}},K=function(t,n,e){!function(t){delete t.llTempImage}(t),x(e,-1),function(t){t&&(t.toLoadCount-=1)}(e),w(t,n.class_loading),n.unobserve_completed&&O(t,e)},Q=function(t,n,e){var i=k(t)||t;B(i)||function(t,n,e){B(t)||(t.llEvLisnrs={});var i="VIDEO"===t.tagName?"loadeddata":"load";q(t,i,n),q(t,"error",e)}(i,(function(o){!function(t,n,e,i){var o=y(n);K(n,e,i),L(n,e.class_loaded),h(n,u),A(e.callback_loaded,n,i),o||$(e,i)}(0,t,n,e),J(i)}),(function(o){!function(t,n,e,i){var o=y(n);K(n,e,i),L(n,e.class_error),h(n,f),A(e.callback_error,n,i),o||$(e,i)}(0,t,n,e),J(i)}))},W=function(t,n,e){!function(t){t.llTempImage=document.createElement("IMG")}(t),Q(t,n,e),function(t,n,e){var i=b(t,n.data_bg),o=b(t,n.data_bg_hidpi),a=r&&o?o:i;a&&(t.style.backgroundImage='url("'.concat(a,'")'),k(t).setAttribute("src",a),S(t,n,e))}(t,n,e),function(t,n,e){var i=b(t,n.data_bg_multi),o=b(t,n.data_bg_multi_hidpi),a=r&&o?o:i;a&&(t.style.backgroundImage=a,function(t,n,e){L(t,n.class_applied),h(t,d),n.unobserve_completed&&O(t,n),A(n.callback_applied,t,e)}(t,n,e))}(t,n,e)},X=function(t,n,e){!function(t){return U.indexOf(t.tagName)>-1}(t)?W(t,n,e):function(t,n,e){Q(t,n,e),P(t,n),S(t,n,e)}(t,n,e)},Y=["IMG","IFRAME","VIDEO"],Z=function(t){return t.use_native&&"loading"in HTMLImageElement.prototype},tt=function(t,n,e){t.forEach((function(t){return function(t){return t.isIntersecting||t.intersectionRatio>0}(t)?function(t,n,e,i){var o=function(t){return I.indexOf(p(t))>=0}(t);h(t,"entered"),L(t,e.class_entered),w(t,e.class_exited),function(t,n,e){n.unobserve_entered&&O(t,e)}(t,e,i),A(e.callback_enter,t,n,i),o||X(t,e,i)}(t.target,t,n,e):function(t,n,e,i){E(t)||(L(t,e.class_exited),function(t,n,e,i){e.cancel_on_exit&&function(t){return p(t)===l}(t)&&"IMG"===t.tagName&&(J(t),function(t){j(t,(function(t){V(t)})),V(t)}(t),function(t){j(t,(function(t){G(t)})),G(t)}(t),w(t,e.class_loading),x(i,-1),m(t),A(e.callback_cancel,t,n,i))}(t,n,e,i),A(e.callback_exit,t,n,i))}(t.target,t,n,e)}))},nt=function(t){return Array.prototype.slice.call(t)},et=function(t){return t.container.querySelectorAll(t.elements_selector)},it=function(t){return function(t){return p(t)===f}(t)},ot=function(t,n){return function(t){return nt(t).filter(E)}(t||et(n))},rt=function(t,e){var o=c(t);this._settings=o,this.loadingCount=0,function(t,n){i&&!Z(t)&&(n._observer=new IntersectionObserver((function(e){tt(e,t,n)}),function(t){return{root:t.container===document?null:t.container,rootMargin:t.thresholds||t.threshold+"px"}}(t)))}(o,this),function(t,e){n&&window.addEventListener("online",(function(){!function(t,n){var e;(e=et(t),nt(e).filter(it)).forEach((function(n){w(n,t.class_error),m(n)})),n.update()}(t,e)}))}(o,this),this.update(e)};return rt.prototype={update:function(t){var n,o,r=this._settings,a=ot(t,r);z(this,a.length),!e&&i?Z(r)?function(t,n,e){t.forEach((function(t){-1!==Y.indexOf(t.tagName)&&function(t,n,e){t.setAttribute("loading","lazy"),Q(t,n,e),P(t,n),h(t,_)}(t,n,e)})),z(e,0)}(a,r,this):(o=a,function(t){t.disconnect()}(n=this._observer),function(t,n){n.forEach((function(n){t.observe(n)}))}(n,o)):this.loadAll(a)},destroy:function(){this._observer&&this._observer.disconnect(),et(this._settings).forEach((function(t){delete t.llOriginalAttrs})),delete this._observer,delete this._settings,delete this.loadingCount,delete this.toLoadCount},loadAll:function(t){var n=this,e=this._settings;ot(t,e).forEach((function(t){O(t,n),X(t,e,n)}))}},rt.load=function(t,n){var e=c(n);X(t,e)},rt.resetStatus=function(t){m(t)},n&&function(t,n){if(n)if(n.length)for(var e,i=0;e=n[i];i+=1)s(t,e);else s(t,n)}(rt,window.lazyLoadOptions),rt}));

},{}],2:[function(require,module,exports){
'use strict';

var _functions = require('./moduls/functions');

var _functions2 = _interopRequireDefault(_functions);

var _DatePickerX = require('./libs/DatePickerX.min');

var _DatePickerX2 = _interopRequireDefault(_DatePickerX);

var _helper = require('./moduls/helper');

var _helper2 = _interopRequireDefault(_helper);

var _loadmoreMedia = require('./moduls/loadmore-media');

var _loadmoreMedia2 = _interopRequireDefault(_loadmoreMedia);

var _matches = require('./moduls/matches');

var _matches2 = _interopRequireDefault(_matches);

var _vote = require('./moduls/vote');

var _vote2 = _interopRequireDefault(_vote);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.onload = function () {

    ////////// preloader

    _functions2.default.remove_preloader();

    ////////////////// search input

    _functions2.default.search();

    //////// tabs 
    _functions2.default.matches_tabs('.switch1', '.inside-tab2');
    _functions2.default.matches_tabs('.summary-box__tab', '.single-match__tab-content');
    _functions2.default.matches_tabs('.switch2', '.tab-switch2');
    _functions2.default.matches_tabs('.table-tabs__tab', '.table-switch-content');
    _functions2.default.matches_tabs('.player-stat-tab', '.player-stat');
    _functions2.default.matches_tabs('.switch3', '.inside-tab');

    _functions2.default.expand_and_collapse('.header__burger', '.header__nav', '-', 'active');
    _functions2.default.expand_and_collapse('.header__mobile-close', '.header__nav', 'active', 'active');
    _functions2.default.expand_and_collapse('.header__more-links', '.header__megamenu', 'active', 'active');
    _functions2.default.expand_and_collapse('.header__search .icon', '.header__search', 'active', 'active');
    _functions2.default.expand_and_collapse('.single-story__open-share', '.social-share-story', 'active', 'active');
    // functions.tabs()

    _functions2.default.select_to_redirect('.comp-select-season');

    ///////////// matches calender


    var calendar = document.getElementById('date-picker');
    if (calendar) {
        _functions2.default.expand_and_collapse('.live-btn', '.matches-today', 'active', 'd-none');

        _DatePickerX2.default.startCalendar();
        // console.log(calendar.value)
        calendar.addEventListener('change', function () {

            window.location = '/matches/' + calendar.value;
        });
    }
    /////// sticky blocks // - check view width
    if (screen.width > 1024) {
        if (document.getElementById('app-content').getAttribute('page-type') != "single-story") {
            _functions2.default.stickySide('.layout__item--mini', '.layout', 90);
        }
        ///////////// share to social 
        if (document.querySelector('.social-share')) {
            _functions2.default.stickySide('.social-share', '.single-wrapper', 105);

            // functions.copytext();
        }
    } else {

            // 
        }
    if (document.querySelector('.social-share') || document.querySelector('.social-share-story')) {
        _functions2.default.social_sharing();
    }

    _functions2.default.replaceMatches();

    ////////// handle img errors 
    var images = document.querySelectorAll('.post-news__img > img');
    if (images.length > 0) {
        // console.log(images)
        _functions2.default.lazy_load();
    }
    ///////// breaking news bottom bar 
    var break_news = document.querySelector('.break-news');
    if (break_news) {
        _functions2.default.hide_break_news();
    }

    if (_helper2.default.page_type() == 'home') {
        _loadmoreMedia2.default.home_filter_news();
        //- vote
        _vote2.default.start_vote();
    }
    if (_helper2.default.page_type() == 'news' || _helper2.default.page_type() == 'video' || _helper2.default.page_type() == 'section' || _helper2.default.page_type() == 'tag' || _helper2.default.page_type() == 'infographic') {
        _loadmoreMedia2.default.load_more();
    }

    if (_helper2.default.page_type() == 'matches') {
        _matches2.default.fill_teams("matches", '.matches-comps', '.matches-teams', '.matches-comps input');
        // console.log('$')
    }
    if (_helper2.default.page_type() == 'team') {
        console.log('$');
        _matches2.default.fill_teams("team", '.select-comp', '.select-season', '.select-comp input');
    }
    if (_helper2.default.page_type() == 'player') {
        console.log('$');
        _matches2.default.fill_teams("player", '.select-comp', '.select-season', '.select-comp input');
    }
};

},{"./libs/DatePickerX.min":4,"./moduls/functions":6,"./moduls/helper":7,"./moduls/loadmore-media":8,"./moduls/matches":9,"./moduls/vote":10}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helper = require('../moduls/helper');

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var card = {};

card.card_theme = function (data) {
    var category_link = '';
    var category_icon = '';
    var category_title = '';
    var video_icon = '';
    var modifier = '';
    if (data.type == 'video') {
        modifier = 'post-news--video';
        video_icon = '<span class="post-news__video">\n        <svg id="Iconly_Bulk_Play" data-name="Iconly/Bulk/Play" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">\n            <g id="Play">\n                <path id="Fill_1" data-name="Fill 1" d="M50,25.012A25,25,0,1,1,25,0,25.03,25.03,0,0,1,50,25.012" fill="#d91d1d"></path>\n                <path id="Fill_4" data-name="Fill 4" d="M17.5,10.012a2.882,2.882,0,0,1-.6,1.774,9.652,9.652,0,0,1-.7.778l-.149.146A37.881,37.881,0,0,1,6.14,19.32,8.687,8.687,0,0,1,3.828,20h-.1A3.136,3.136,0,0,1,.969,18.35a13.655,13.655,0,0,1-.6-2.211A44.072,44.072,0,0,1,0,9.988,38.032,38.032,0,0,1,.423,3.645,15.954,15.954,0,0,1,.87,1.871,3.189,3.189,0,0,1,2.262.365,3.555,3.555,0,0,1,3.828,0,8.607,8.607,0,0,1,5.891.559,38.06,38.06,0,0,1,16.033,7.266c.348.34.721.756.82.851a3.024,3.024,0,0,1,.646,1.9" transform="translate(17.5 15)" fill="#fff"></path>\n            </g>\n        </svg></span>';
    } else {
        modifier = '';
        video_icon = '';
    }
    if (data.type == 'news') {
        category_icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="15" viewBox="0 0 30 15">\n        <defs>\n            <linearGradient id="linear-gradient" x1="0.92" y1="0.5" x2="0" y2="0.5" gradientUnits="objectBoundingBox">\n                <stop offset="0" stop-color="#24a557"></stop>\n                <stop offset="1" stop-color="#bafb3c"></stop>\n            </linearGradient>\n        </defs>\n        <rect id="your_logo_here" data-name="your logo here" width="30" height="15" rx="2" fill="url(#linear-gradient)"></rect>\n    </svg>';
        category_link = data.sections.slug;
        category_title = data.sections.title;
    } else if (data.type == 'infographic') {
        category_icon = '<svg id="Iconly_Bulk_Graph" data-name="Iconly/Bulk/Graph" xmlns="http://www.w3.org/2000/svg" width="19.02" height="19.02" viewBox="0 0 19.02 19.02">\n        <g id="Graph">\n          <path id="Path" d="M7.753.613a1.008,1.008,0,0,1,.095.313l.265,3.937h0l.131,1.979a2.035,2.035,0,0,0,.095.6.992.992,0,0,0,.956.6l6.331-.414a1.044,1.044,0,0,1,.736.285,1.008,1.008,0,0,1,.3.579l.011.133a8.256,8.256,0,0,1-6.547,7.435A8.422,8.422,0,0,1,1.009,12,7.812,7.812,0,0,1,.061,9.013,5.723,5.723,0,0,1,0,8.075,8.269,8.269,0,0,1,6.642.013,1.038,1.038,0,0,1,7.753.613Z" transform="translate(0 2.769)" fill="#171c19"></path>\n          <path id="Path-2" data-name="Path" d="M.807,0A8.981,8.981,0,0,1,9.49,7.429l-.007.031h0l-.019.045,0,.124a.761.761,0,0,1-.182.449.787.787,0,0,1-.44.258l-.1.014-7.3.473a.872.872,0,0,1-.666-.216A.84.84,0,0,1,.5,8.1L.006.8a.116.116,0,0,1,0-.076A.746.746,0,0,1,.252.2.775.775,0,0,1,.807,0Z" transform="translate(9.53)" fill="#17a654" opacity="0.4"></path>\n        </g>\n      </svg>';
        category_link = 'infographics';
        category_title = 'إنفوجراف';
    }
    var link = location.origin + '/' + data.slug;
    var html = '\n    <div class="post-news post-news--card post-news--mobile ' + modifier + '">\n        <a class="post-news__link" href="' + link + '" title="' + data.title + '"></a>\n        <div class="post-news__img"> \n            <img  src="' + _helper2.default.image_server + data.images[0].url + '" alt="' + data.title + '" title="' + data.title + '" onerror="this.src=\'img/no-img.png\';">\n            ' + video_icon + '\n        </div>\n        <div class="post-news__data">';

    if (data.type != 'video') {
        html += '<a class="post-news__category" href="' + location.origin + '/' + category_link + '" title="' + data.title + '">\n            <span class="icon">\n            ' + category_icon + '\n            </span>\n            <p class="text-gradient">\n                ' + category_title + '\n            </p>\n        </a>';
    }

    html += '<p class="post-news__title">' + data.title + '</p>\n        <div class="post-news__info">\n            <div class="post-news__time">' + data.published_date + '</div>\n            <div class="post-news__views">\n                <span class="icon">\n                    <svg id="Iconly_Light-outline_Show" data-name="Iconly/Light-outline/Show" xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">\n                    <g id="Show" transform="translate(1.79 3.58)">\n                        <path id="Show-2" data-name="Show" d="M8.692,14.316c-3.5-.116-6.676-2.585-8.51-6.6L.054,7.424a.665.665,0,0,1,0-.527A12.774,12.774,0,0,1,3.807,1.843,8.357,8.357,0,0,1,8.952,0l.256,0C12.7.121,15.884,2.591,17.718,6.61l.125.28a.667.667,0,0,1,0,.533,12.783,12.783,0,0,1-3.754,5.054,8.357,8.357,0,0,1-5.144,1.843ZM4.771,2.791a11.028,11.028,0,0,0-3.226,4.07l-.14.3.151.323a11.191,11.191,0,0,0,3.025,3.9,7.142,7.142,0,0,0,3.9,1.584l.238.012.23,0,.228,0a7.107,7.107,0,0,0,4.148-1.606,11.336,11.336,0,0,0,3.156-4.2l.009-.022A11.452,11.452,0,0,0,13.43,3.024,7.184,7.184,0,0,0,9.411,1.35l-.238-.012-.214,0A7.005,7.005,0,0,0,4.771,2.791Zm.678,4.37a3.5,3.5,0,1,1,3.5,3.478A3.494,3.494,0,0,1,5.449,7.161Zm1.343,0A2.158,2.158,0,1,0,8.95,5.016,2.154,2.154,0,0,0,6.792,7.161Z" transform="translate(0)" fill="#17a654"></path>\n                    </g>\n                    </svg>\n                </span>\n                <p>' + data.hits + '</p>\n            </div>\n        </div>\n        </div>\n    </div>\n    ';

    return html;
};

exports.default = card;

},{"../moduls/helper":7}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * DatePickerX
 *
 * Cool light visual date picker on pure JavaScript
 * Browsers support: Chrome 45+, FireFox 40+, Safari 8+, IE10+, iOS Safari 8+, Android Browser 4.4+
 *
 * @author    Alexander Krupko <alex@avrora.team>
 * @copyright 2016 Avrora Team www.avrora.team
 * @license   MIT
 * @tutorial  http://datepickerx.avrora.team
 * @version   1.0.4
 */

var datePicker = {};

!function () {
    'use strict';
    function a(a, b, c, d, e) {
        b = b || [], !Array.isArray(b) && (b = [b]);for (var f = document.createElement(a), g = b.length; g--; f.classList.add(b[g])) {}return e && (f.title = e), f.innerHTML = d || '', c instanceof Element && c.appendChild(f), f;
    }function b(a) {
        return a = a || new Date(), new Date(a.getFullYear(), a.getMonth(), a.getDate());
    }function c(c) {
        function f() {
            var a = n.minDate;return a instanceof HTMLInputElement && (a = a.DatePickerX.getValue(!0), a = null === a ? n.minDate.DatePickerX.getMinDate() : new Date(a), a.setDate(a.getDate() + 1)), b(a);
        }function g() {
            var a = n.maxDate;return a instanceof HTMLInputElement && (a = a.DatePickerX.getValue(!0), a = null === a ? n.maxDate.DatePickerX.getMaxDate() : new Date(a), a.setDate(a.getDate() - 1)), b(a);
        }function h() {
            o.container = a('div', 'date-picker-x');var b = a('div', 'dpx-title-box', o.container);if (o.prevTitle = a('span', 'dpx-prev', b, '&#x276e;'), o.title = a('span', 'dpx-title', b), o.nextTitle = a('span', 'dpx-next', b, '&#x276f;'), o.content = a('div', 'dpx-content-box', o.container), c.nextElementSibling ? c.parentNode.insertBefore(o.container, c.nextElementSibling) : c.parentNode.appendChild(o.container), n.todayButton || n.clearButton) {
                var d = a('div', 'dpx-btns', o.container);n.todayButton && (o.today = a('span', ['dpx-item', 'dpx-today'], d, n.todayButtonLabel, n.todayButtonLabel)), n.clearButton && (o.clear = a('span', ['dpx-item', 'dpx-clear'], d, n.clearButtonLabel, n.clearButtonLabel));
            }
        }function i(a, b) {
            var c = { d: a.getDate(), dd: a.getDate(), D: a.getDay(), m: a.getMonth() + 1, mm: a.getMonth() + 1, M: a.getMonth(), MM: a.getMonth(), yy: a.getFullYear().toString().substr(-2), yyyy: a.getFullYear() };return c.dd < 10 && (c.dd = '0' + c.dd), c.mm < 10 && (c.mm = '0' + c.mm), c.D = n.weekDayLabels[c.D ? c.D - 1 : 6], c.M = n.shortMonthLabels[c.M], c.MM = n.singleMonthLabels[c.MM], b.replace(/(?:[dmM]{1,2}|D|yyyy|yy)/g, function (a) {
                return 'undefined' != typeof c[a] ? c[a] : a;
            });
        }function j() {
            return o.container.classList.contains('active');
        }function k(a) {
            c.addEventListener('click', function (a) {
                if (!j()) {
                    a.stopPropagation(), r = 2, l(), o.container.classList.add('active'), o.container.classList.remove('to-top');var b = o.container.getBoundingClientRect();b.bottom > window.innerHeight && b.top + c.offsetHeight > o.container.offsetHeight && (o.container.classList.add('to-top'), o.container.getBoundingClientRect().top < 0 && o.container.classList.remove('to-top')), e && e !== o.container && e.classList.remove('active'), e = o.container;
                }
            }), window.addEventListener('click', function () {
                e && e.classList.remove('active');
            }), o.container.addEventListener('click', function (a) {
                a.stopPropagation(), a.preventDefault();
            }), o.content.addEventListener('click', function (b) {
                if (2 === r) a.setValue(b.target.dpxValue) && o.container.classList.remove('active');else {
                    var c = f(),
                        d = g();c.setDate(1), d.setDate(1), r || (c.setMonth(0), d.setMonth(0)), b.target.dpxValue >= c.getTime() && b.target.dpxValue <= d.getTime() && ++r && l(b.target.dpxValue);
                }
            }), o.prevTitle.addEventListener('click', function () {
                l(this.dpxValue);
            }), o.nextTitle.addEventListener('click', function () {
                l(this.dpxValue);
            }), o.title.addEventListener('click', function () {
                r && r-- && l(this.dpxValue);
            }), o.today && o.today.addEventListener('click', function () {
                !this.classList.contains('dpx-disabled') && a.setValue(b()) && o.container.classList.remove('active');
            }), o.clear && o.clear.addEventListener('click', function () {
                a.setValue(null) && o.container.classList.remove('active');
            });
        }function l(c) {
            o.content.innerHTML = '';var d = f(),
                e = g(),
                h = b();n.todayButton && o.today.classList[h >= d && h <= e ? 'remove' : 'add']('dpx-disabled'), r < 2 && (d.setDate(1), e.setDate(1), r || (d.setMonth(0), e.setMonth(0))), d = d.getTime(), e = e.getTime(), c = b(new Date(c || q || Date.now())), c.getTime() < d ? c = new Date(d) : c.getTime() > e && (c = new Date(e));var i = c.getMonth(),
                j = c.getFullYear(),
                k = 10 * Math.floor(j / 10);if (c = new Date(r ? j : k, r < 2 ? 0 : i), o.title.innerHTML = r ? (2 === r ? n.singleMonthLabels[i] + ' ' : '') + j : k + ' - ' + (k + 9), o.title.dpxValue = c.getTime(), o.title.title = 2 === r ? j : k + ' - ' + (k + 9), o.prevTitle.classList[c.getTime() <= d ? 'add' : 'remove']('dpx-disabled'), 2 === r ? c.setMonth(i - 1) : c.setFullYear(r ? j - 1 : k - 10), o.prevTitle.title = r ? (2 === r ? n.singleMonthLabels[c.getMonth()] + ' ' : '') + c.getFullYear() : k - 10 + ' - ' + (k - 1), o.prevTitle.dpxValue = c.getTime(), 2 === r ? c.setMonth(c.getMonth() + 2) : c.setFullYear(r ? j + 1 : k + 20), o.nextTitle.classList[c.getTime() > e ? 'add' : 'remove']('dpx-disabled'), o.nextTitle.title = r ? (2 === r ? n.singleMonthLabels[c.getMonth()] + ' ' : '') + c.getFullYear() : k + 10 + ' - ' + (k + 19), o.nextTitle.dpxValue = c.getTime(), 2 === r ? c.setMonth(c.getMonth() - 1) : c.setFullYear(r ? j : k), 2 === r) {
                var l = n.weekDayLabels.length;!n.mondayFirst && --l && a('span', ['dpx-item', 'dpx-weekday', 'dpx-weekend'], o.content, n.weekDayLabels[6]);for (var m = 0; m < l; ++m) {
                    var p = ['dpx-item', 'dpx-weekday'];m > 4 && p.push('dpx-weekend'), a('span', p, o.content, n.weekDayLabels[m]);
                }
            }if (2 === r) {
                var s = c.getDay();c.setDate(n.mondayFirst ? -(s ? s - 2 : 5) : -s + 1);
            } else r ? c.setMonth(c.getMonth() - 2) : c.setFullYear(k - 3);var t = q;r < 2 && (null !== t && (t = new Date(t), t.setDate(1), !r && t.setMonth(0), t = t.getTime()), h.setDate(1), !r && h.setMonth(0)), h = h.getTime(), o.container.setAttribute('data-dpx-type', ['year', 'month', 'day'][r]);for (var u = ['getFullYear', 'getMonth', 'getDate'][r], v = ['setFullYear', 'setMonth', 'setDate'][r], w = 2 === r ? 42 : 16; w--; c[v](c[u]() + 1)) {
                var p = ['dpx-item'],
                    x = r ? n.singleMonthLabels[c.getMonth()] + ' ' : '';2 === r && (x += c.getDate() + ', '), x += c.getFullYear(), (r ? 2 === r ? c.getMonth() !== i : c.getFullYear() !== j : c.getFullYear() < k || c.getFullYear() > k + 9) && p.push('dpx-out'), 2 === r && (6 === c.getDay() || 0 === c.getDay()) && p.push('dpx-weekend'), c.getTime() === h && p.push('dpx-current'), c.getTime() === q && p.push('dpx-selected'), (c.getTime() < d || c.getTime() > e) && p.push('dpx-disabled');var y = r ? 2 === r ? c.getDate() : n.shortMonthLabels[c.getMonth()] : c.getFullYear(),
                    z = a('span', p, o.content, y, x);z.dpxValue = c.getTime();
            }
        }function m(a, b) {
            if ('undefined' == typeof n[a]) return console.error('DatePickerX, setOption: Option doesn\'t exist.') && !1;if ('minDate' === a || 'maxDate' === a) {
                if (!(b instanceof HTMLInputElement) && (!(b instanceof Date) && (b = new Date(b)), isNaN(b))) return console.error('DatePickerX, setOption: Invalid date value.') && !1;
            } else {
                if (_typeof(n[a]) != (typeof b === 'undefined' ? 'undefined' : _typeof(b))) return console.error('DatePickerX, setOption: Option has invalid type.') && !1;if (Array.isArray(n[a])) {
                    if (b.length < n[a].length) return console.warn('DatePickerX, setOption: Invalid option length.') && !1;b = b.slice(0, n[a].length);
                }
            }n[a] = b;
        }var n = {},
            o = {},
            p = !1,
            q = null,
            r = 2;return { init: function init(a) {
                if (a = a || {}, p) return console.error('DatePickerX, init: Date picker have already inited.') && !1;p = !0, n = {};for (var b in d) {
                    n[b] = d[b];
                }if ('object' != (typeof a === 'undefined' ? 'undefined' : _typeof(a))) console.error('DatePickerX, init: Initial options must be an object.');else for (var b in a) {
                    m(b, a[b]);
                }return c.parentNode.classList.add('date-picker-x-container'), c.classList.add('date-picker-x-input'), c.readOnly = !0, h(), k(this), !0;
            }, remove: function remove() {
                return p ? (c.parentNode.removeChild(o.container), c.classList.remove('date-picker-x-input'), c.readOnly = p = !1, !0) : console.error('DatePickerX, remove: Date picker doesn\'t init yet.') && !1;
            }, setValue: function setValue(a, b) {
                if (!p) return console.error('DatePickerX, remove: Date picker doesn\'t init yet.') && !1;if (null === a) q = null, c.value = '';else {
                    if (!(a instanceof Date) && (a = new Date(a)), isNaN(a)) return console.error('DatePickerX, setValue: Can\'t convert argument to date.') && !1;if (!b && (a.getTime() < f().getTime() || a.getTime() > g().getTime())) return console.error('DatePickerX, setValue: Date out of acceptable range.') && !1;q = a.getTime(), c.value = i(a, n.format);
                }var d = document.createEvent('Event');return d.initEvent('change', !0, !0), c.dispatchEvent(d), j() && l(), !0;
            }, getValue: function getValue(a) {
                return !p && console.error('DatePickerX, getValue: Date picker doesn\'t init yet.'), a ? q : null === q ? '' : i(new Date(q), n.format);
            }, getMinDate: function getMinDate() {
                var a = n.minDate;return a instanceof HTMLInputElement && (a = a.DatePickerX.getMinDate()), b(a);
            }, getMaxDate: function getMaxDate() {
                var a = n.maxDate;return a instanceof HTMLInputElement && (a = a.DatePickerX.getMaxDate()), b(a);
            } };
    }var d = { mondayFirst: !0, format: 'yyyy/mm/dd', minDate: new Date(0, 0), maxDate: new Date(9999, 11, 31), weekDayLabels: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'St', 'Su'], shortMonthLabels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], singleMonthLabels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'], todayButton: !0, todayButtonLabel: 'Today', clearButton: !0, clearButtonLabel: 'Clear' },
        e = null,
        f = [],
        g = [];Object.defineProperty(HTMLInputElement.prototype, 'DatePickerX', { get: function get() {
            var a = f.indexOf(this);return a === -1 && (a = f.push(this) - 1, g.push(new c(this))), g[a];
        }, set: function set() {} }), window.DatePickerX = { setDefaults: function setDefaults(a) {
            if ('object' != (typeof a === 'undefined' ? 'undefined' : _typeof(a))) return console.error('DatePickerX, setDefaults: Invalid option type.') && !1;for (var b in a) {
                _typeof(a[b]) == _typeof(d[b]) && (Array.isArray(d[b]) ? a[b].length >= d[b].length && (d[b] = a[b].slice(0, d[b].length)) : d[b] = a[b]);
            }return !0;
        } };
}();

datePicker.startCalendar = function () {
    var myDatepicker = document.getElementById('date-picker');

    if (myDatepicker) {
        //console.log(myDatepicker)
        myDatepicker.DatePickerX.init({

            mondayFirst: false,
            format: 'yyyy/mm/dd',
            minDate: new Date(0, 0),
            maxDate: new Date(9999, 11, 31),
            weekDayLabels: ['الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة', 'السبت', 'الأحد'],
            shortMonthLabels: ['يناير', 'فبراير', 'مارس', 'إبريل', 'مايو', 'يونيه', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
            singleMonthLabels: ['يناير', 'فبراير', 'مارس', 'إبريل', 'مايو', 'يونيه', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
            todayButton: false,
            todayButtonLabel: 'Today',
            clearButton: false,
            clearButtonLabel: 'Clear'

        });
        //matches.get_data_matches(false,myDatepicker.value)
    }
};

// weekDayLabels: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'St', 'Su'],
//             shortMonthLabels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
//             singleMonthLabels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],


exports.default = datePicker;

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * float-sidebar - Lightweight, vanilla javascript library for making smart float sidebars
 * @version v1.1.0
 * @link https://github.com/vursen/FloatSidebar.js
 * @author Sergey Vinogradov
 * @license The MIT License (MIT)
 */
var sticky = {};

!function (t, n) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) ? module.exports = n() : "function" == typeof define && define.amd ? define([], n) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports.FloatSidebar = n() : t.FloatSidebar = n();
}(window, function () {
  return function (t) {
    var n = {};function e(i) {
      if (n[i]) return n[i].exports;var o = n[i] = { i: i, l: !1, exports: {} };return t[i].call(o.exports, o, o.exports, e), o.l = !0, o.exports;
    }return e.m = t, e.c = n, e.d = function (t, n, i) {
      e.o(t, n) || Object.defineProperty(t, n, { configurable: !1, enumerable: !0, get: i });
    }, e.r = function (t) {
      Object.defineProperty(t, "__esModule", { value: !0 });
    }, e.n = function (t) {
      var n = t && t.__esModule ? function () {
        return t.default;
      } : function () {
        return t;
      };return e.d(n, "a", n), n;
    }, e.o = function (t, n) {
      return Object.prototype.hasOwnProperty.call(t, n);
    }, e.p = "", e(e.s = 0);
  }([function (t, n, e) {
    "use strict";
    e.r(n);var i,
        o = "START",
        r = "TOP_FIXED",
        s = "UNFIXED",
        u = "BOTTOM_FIXED",
        a = "FINISH";function p(t, n, e) {
      return n in t ? Object.defineProperty(t, n, { value: e, enumerable: !0, configurable: !0, writable: !0 }) : t[n] = e, t;
    }var c,
        f = (p(i = {}, o, [{ to: a, when: function when(t) {
        return [!0 === t.isSideInnerFitsPath, t.viewportTop + t.sideInnerHeight >= t.finishPoint];
      } }, { to: u, when: function when(t) {
        return [!0 === t.isSideInnerFitsPath, !1 === t.isSideInnerFitsViewport, t.viewportBottom >= t.sideInnerBottom + t.bottomSpacing];
      } }, { to: r, when: function when(t) {
        return [!0 === t.isSideInnerFitsPath, !0 === t.isSideInnerFitsViewport, t.viewportTop >= t.startPoint - t.topSpacing];
      } }]), p(i, r, [{ to: o, when: function when(t) {
        return [!1 === t.isSideInnerFitsPath];
      } }, { to: o, when: function when(t) {
        return [t.viewportTop <= t.startPoint - t.topSpacing];
      } }, { to: a, when: function when(t) {
        return [t.sideInnerBottom >= t.finishPoint];
      } }, { to: s, when: function when(t) {
        return ["down" === t.scrollDirection, !1 === t.isSideInnerFitsViewport];
      } }]), p(i, s, [{ to: o, when: function when(t) {
        return [!1 === t.isSideInnerFitsPath];
      } }, { to: r, when: function when(t) {
        return [t.viewportTop <= t.sideInnerTop - t.topSpacing];
      } }, { to: r, when: function when(t) {
        return [!0 === t.isSideInnerFitsViewport, t.viewportBottom >= t.sideInnerBottom + t.bottomSpacing];
      } }, { to: u, when: function when(t) {
        return [!1 === t.isSideInnerFitsViewport, t.viewportBottom >= t.sideInnerBottom + t.bottomSpacing];
      } }]), p(i, u, [{ to: o, when: function when(t) {
        return [!1 === t.isSideInnerFitsPath];
      } }, { to: s, when: function when(t) {
        return ["up" === t.scrollDirection];
      } }, { to: r, when: function when(t) {
        return [!0 === t.isSideInnerFitsViewport];
      } }, { to: a, when: function when(t) {
        return [t.sideInnerBottom >= t.finishPoint];
      } }]), p(i, a, [{ to: o, when: function when(t) {
        return [!1 === t.isSideInnerFitsPath];
      } }, { to: u, when: function when(t) {
        return [t.sideInnerBottom + t.bottomSpacing <= t.finishPoint, t.viewportBottom <= t.finishPoint];
      } }, { to: r, when: function when(t) {
        return [t.viewportTop <= t.sideInnerTop - t.topSpacing];
      } }]), i);function d(t, n, e) {
      return n in t ? Object.defineProperty(t, n, { value: e, enumerable: !0, configurable: !0, writable: !0 }) : t[n] = e, t;
    }var l = (d(c = {}, o, function (t, n) {
      var e = n.$sideInner;e.style.position = "absolute", e.style.top = "0", e.style.bottom = "auto";
    }), d(c, r, function (t, n) {
      var e = n.$sideInner;e.style.position = "fixed", e.style.top = t.topSpacing + "px", e.style.bottom = "auto";
    }), d(c, s, function (t, n) {
      var e = n.$sideInner;e.style.position = "absolute", e.style.top = t.sideInnerTop - t.startPoint + "px", e.style.bottom = "auto";
    }), d(c, u, function (t, n) {
      var e = n.$sideInner;e.style.position = "fixed", e.style.top = "auto", e.style.bottom = t.bottomSpacing + "px";
    }), d(c, a, function (t, n) {
      var e = n.$sideInner;e.style.position = "absolute", e.style.top = "auto", e.style.bottom = "0";
    }), c);var h = function h(t) {
      var n = t.actions,
          e = t.transitions,
          i = t.initialState;return { findTransitionFor: function findTransitionFor() {
          for (var t = arguments.length, n = Array(t), o = 0; o < t; o++) {
            n[o] = arguments[o];
          }return e[i].find(function (t) {
            return t.when.apply(void 0, n).every(function (t) {
              return t;
            });
          });
        }, performTransition: function performTransition(t) {
          var e = t.to;return function () {
            i = e, n[e].apply(n, arguments);
          };
        } };
    };var v = function v(t) {
      var n = void 0;return function () {
        n || (n = requestAnimationFrame(function () {
          n = null, t();
        }));
      };
    },
        w = function w(t) {
      var n = t.clientHeight || t.innerHeight,
          e = t.scrollTop || t.pageYOffset;return { top: e, bottom: e + n, height: n };
    },
        m = function m(t, n) {
      var e = t.getBoundingClientRect();return { top: e.top + n, bottom: e.bottom + n, height: e.height };
    };var g = function g(t, n) {
      var e = n.$viewport,
          i = n.$relative,
          o = n.$sideInner,
          r = n.$sideOuter,
          s = n.topSpacing,
          u = n.bottomSpacing,
          a = {},
          p = function p() {
        var t,
            n = w(e),
            p = m(o, n.top),
            c = m(r, n.top),
            f = m(i, n.top),
            d = (t = n.top, a.viewportTop < t ? "down" : a.viewportTop > t ? "up" : "notChanged"),
            l = c.top,
            h = f.bottom,
            v = h - l,
            g = p.height + s + u < n.height,
            b = p.height < v,
            I = Math.max(p.height, v);return { startPoint: l, finishPoint: h, topSpacing: s, bottomSpacing: u, scrollDirection: d, isSideInnerFitsPath: b, isSideInnerFitsViewport: g, sideOuterHeight: I, viewportTop: n.top, viewportBottom: n.bottom, sideInnerTop: p.top, sideInnerBottom: p.bottom, sideInnerHeight: p.height };
      },
          c = v(function () {
        var n = p();t(a, n), a = n;
      });return { start: function start() {
          e.addEventListener("scroll", c), e.addEventListener("resize", c), c();
        }, stop: function stop() {
          e.removeEventListener("scroll", c), e.removeEventListener("resize", c);
        }, tick: c };
    };n.default = function (t) {
      var n = t.viewport || window,
          e = t.sidebar,
          i = t.sidebarInner || e.firstElementChild,
          r = t.relative,
          s = t.topSpacing || 0,
          u = t.bottomSpacing || 0,
          a = h({ actions: l, transitions: f, initialState: o }),
          p = g(function (t, n) {
        var o = a.findTransitionFor(n);o && a.performTransition(o)(n, { $sideInner: i, $sideOuter: e, $relative: r }), c(t, n);
      }, { $viewport: n, $sideOuter: e, $sideInner: i, $relative: r, topSpacing: s, bottomSpacing: u }),
          c = function c(t, n) {
        Math.abs((t.sideOuterHeight || 0) - n.sideOuterHeight) >= 1 && (e.style.height = n.sideOuterHeight + "px");
      };return requestAnimationFrame(function () {
        e.style.willChange = "height", i.style.width = "inherit", i.style.transform = "translateZ(0)", i.style.willChange = "transform", p.start();
      }), { forceUpdate: function forceUpdate() {
          p.tick();
        }, destroy: function destroy() {
          p.stop();
        } };
    };
  }]).default;
});

exports.default = sticky;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _floatSidebar = require("../libs/float-sidebar.min");

var _floatSidebar2 = _interopRequireDefault(_floatSidebar);

var _vanillaLazyload = require("vanilla-lazyload");

var _vanillaLazyload2 = _interopRequireDefault(_vanillaLazyload);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var functions = {};

var checkWidth = window.matchMedia("(min-width: 1025px)");

functions.tabs = function () {
	var bindAll = function bindAll() {
		var tabs = document.querySelectorAll('[data-tab]');
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].addEventListener('click', change, false);
		}
	};
	var clear = function clear() {
		var tabs = document.querySelectorAll('[data-tab]');
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].classList.remove('active');
			var id = tabs[i].getAttribute('data-tab');
			document.getElementById(id).classList.remove('active');
		}
	};
	var change = function change(e) {
		clear();
		e.target.classList.add('active');
		var id = e.currentTarget.getAttribute('data-tab');
		document.getElementById(id).classList.add('active');
	};
	bindAll();
};
functions.remove_preloader = function () {
	setTimeout(function () {
		document.querySelector('.pre-loader').classList.remove('active');
	}, 500);
};

// -- toggle active 2 elements or 1 element if founded
functions.expand_and_collapse = function (el_1, el_2, class_1, class_2) {
	el_1 = document.querySelector(el_1);
	if (el_1 && el_2) {
		el_1.onclick = function () {
			el_1.classList.toggle(class_1);
			document.querySelector(el_2).classList.toggle(class_2);
		};
	} else if (el_1 && !el_2) {
		el_1.onclick = function () {
			el_1.classList.toggle(class_1);
		};
	} else {}
};

//-- Sticky
functions.stickySide = function (aside, box, top) {
	var sidebar = document.querySelector(aside);
	var content = document.querySelector(box);
	if (checkWidth.matches) {
		var floatSidebar = new _floatSidebar2.default({
			sidebar: sidebar,
			relative: content,
			topSpacing: top,
			bottomSpacing: 20
		});
	}
};
//-- toggle match tabs
functions.matches_tabs = function (_tab, _content) {
	// console.log(_tab)
	// console.log(_content)
	var tab = document.querySelectorAll(_tab);

	if (tab) {
		(function () {
			var content = document.querySelectorAll(_content);

			var _loop = function _loop(i) {
				tab[i].onclick = function () {
					for (var x = 0; x < tab.length; x++) {

						tab[x].classList.remove('active');
						content[x].classList.remove('active');
					}
					tab[i].classList.add('active');
					content[i].classList.add('active');
				};
			};

			for (var i = 0; i < tab.length; i++) {
				_loop(i);
			}
		})();
	}
};

functions.lazy_load = function () {
	var lazyLoadInstance = new _vanillaLazyload2.default({
		// Your custom settings go here
	});
};

functions.hide_break_news = function () {
	var div = document.querySelector('.break-news');
	var close = document.querySelector('.break-news__close');
	close.addEventListener('click', function () {
		div.style.display = 'none';
	});
};
functions.social_sharing = function () {

	var copyText = document.querySelector(".cp-share-input"),
	    hint_message = document.querySelector(".action-message");

	copyText.addEventListener("click", function () {
		copyText.select();
		copyText.setSelectionRange(0, 99999);
		document.execCommand("copy");
		document.querySelector(".action-message").classList.remove("active");
		hint_message.classList.add("active");
		setTimeout(function () {
			document.querySelector(".action-message").classList.remove("active");
		}, "2000");
	});

	var fb_share = document.querySelector(".fb-share"),
	    twtr_share = document.querySelector(".tw-share"),
	    wts_share = document.querySelector(".wt-share");

	fb_share.addEventListener("click", function (e) {
		e.preventDefault();
		window.open("http://www.facebook.com/sharer/sharer.php?u=" + window.location.href, "_blank", "width=600,height=600");
	});

	twtr_share.addEventListener("click", function (e) {
		e.preventDefault();
		window.open("https://twitter.com/intent/tweet?text=" + window.location.href, "_blank", "width=400,height=400");
	});

	wts_share.addEventListener("click", function (e) {
		e.preventDefault();
		window.open("whatsapp://send?text=" + window.location.href + "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600", "_blank", "width=400,height=400");
	});
};
functions.search = function () {

	var searchInput = document.querySelector('.header__search-box input'),
	    submit = document.querySelector('.enter-search');
	if (searchInput) {
		searchInput.onsearch = function () {
			if (searchInput.value.length > 2) {

				window.location.href = "/Search?q=" + searchInput.value;
			}
		};
	}
	if (submit) {
		submit.onclick = function () {
			if (searchInput.value.length > 2) {

				window.location.href = "/Search?q=" + searchInput.value;
			}
		};
	}
};
functions.replaceMatches = function () {
	var ht_content = "",
	    content_holder = document.querySelector(".switch-matches-view"),
	    content_replace = document.querySelector(".home-matches");
	if (content_replace) {
		ht_content = content_holder.innerHTML;
		if (screen.width < 1024 && content_replace.innerHTML == "") {
			content_replace.innerHTML = ht_content;
		}
		window.addEventListener('resize', function () {

			if (screen.width > 1024) {

				if (content_replace.innerHTML != "") {
					content_holder.innerHTML = ht_content;
					content_replace.innerHTML = "";
					// console.log("ss")
				} else {
					content_holder.innerHTML = ht_content;
					content_replace.innerHTML = "";
				}
			} else {
				if (content_replace.innerHTML == "") {
					content_holder.innerHTML = "";
					content_replace.innerHTML = ht_content;
					// console.log("we")
				}
			}
		});
	}
};
functions.select_to_redirect = function (_input) {
	var radio_btn = document.querySelectorAll(_input);
	// _link = radio_btn.getAttribute(`data-url`)

	var _loop2 = function _loop2(i) {
		radio_btn[i].addEventListener("change", function () {
			// e.preventDefault()
			location.href = radio_btn[i].getAttribute('data-url');
		});
	};

	for (var i = 0; i < radio_btn.length; i++) {
		_loop2(i);
	}
};

exports.default = functions;

},{"../libs/float-sidebar.min":5,"vanilla-lazyload":1}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var helper = {};

// helper._baseUrl = 'https://newweb.so3ody.com/'
helper._baseUrl = location.origin;

helper.get_data = function (url) {
    return fetch(helper._baseUrl + '/api/' + url);
};

helper.post_data = function (url, data) {
    var otherParam = {
        headers: {
            'content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(data),
        method: 'POST'
    };
    return fetch(helper._baseUrl + '/api/' + url, otherParam);
};

helper.image_server = 'https://so3ody.s3.amazonaws.com/images';

helper.add_loader = function (parent) {
    parent = document.querySelector(parent);
    parent.innerHTML += '<div class="lds-ellipsis">\n    <div></div>\n    <div></div>\n    <div></div>\n</div>';
};

helper.remove_loader = function (parent, target) {
    var box = document.querySelector('' + parent);
    target = document.querySelector('' + target);
    if (target) {
        box.removeChild(target);
    }
};

helper.page_type = function () {
    var app = document.getElementById('app-content');
    var type = app.getAttribute('page-type');
    return type;
};

helper.section_id = function () {
    var div = document.getElementById('sectionId');
    var sec = div.getAttribute('value');
    return sec;
};
helper.tag_id = function () {
    var div = document.getElementById('tagId');
    var tag = div.getAttribute('value');
    return tag;
};

exports.default = helper;

},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helper = require("./helper");

var _helper2 = _interopRequireDefault(_helper);

var _media_card = require("../components/media_card");

var _media_card2 = _interopRequireDefault(_media_card);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var loadmore_media = {};

var action_type = 'append';
var page_name = '';
var page_index = 2;
var page_size = 9;
var section_id = '';
var tag_id = '';
var page_type = _helper2.default.page_type();

var btn = document.querySelector('.btn--viewall');
var box = document.querySelector('.layout__item--large .row');

loadmore_media.load_more = function () {
    if (page_name == 'home') {
        return false;
    }
    if (btn) {
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            action_type = 'append';
            var url = '';
            if (page_type == 'news') {
                //-- News page
                page_name = 'allNews';
            } else if (page_type == 'infographic') {
                //-- Infographics page
                page_name = 'infographic';
            } else if (page_type == 'section') {
                //-- Section
                page_name = 'section';
                section_id = "&sectionId=" + _helper2.default.section_id();
            } else if (page_type == 'tag') {
                //-- Tag
                page_name = 'tag';
                tag_id = "&tagId=" + _helper2.default.tag_id();
            } else if (page_type == 'video') {
                //-- Video
                page_name = 'video';
            }
            url = "pageName=" + page_name + "&limit=" + page_size + "&pageIndex=" + page_index + section_id + tag_id;
            loadmore_media.get_data(url);
        });
    }
};

loadmore_media.looper = function (collection, parent, grid, action_type) {
    console.log(action_type);
    if (collection.length > 0) {

        for (var i = 0; i < collection.length; i++) {
            parent.innerHTML += "<div class=\"" + grid + "\">" + _media_card2.default.card_theme(collection[i]) + "</div>";

            // if (action_type == 'append') {
            //     parent.innerHTML += `<div class="${grid}">${card.card_theme(collection[i])}</div>`;
            // } else if (action_type == 'change') {
            //     parent.innerHTML += `<div class="${grid}">${card.card_theme(collection[i])}</div>`;
            // }
        }
    } else {
            // btn.style.display = 'none';
            // box.innerHTML += helper.no_data()
        }
};

loadmore_media.get_data = function (url) {
    if (action_type == 'change') {
        box.innerHTML = '';
    }
    _helper2.default.add_loader('.layout__item--large .row');
    _helper2.default.get_data('more-news?' + url).then(function (res) {
        return res.json();
    }).then(function (data) {
        _helper2.default.remove_loader('.layout__item--large .row', '.layout__item--large .row .lds-ellipsis');
        if (page_type == 'video') {
            data = data.latestVideos;
        } else {
            data = data.latestNews;
        }
        loadmore_media.looper(data, box, 'col-md-4 col-sm-6', action_type);
        page_index++;
    });
};

loadmore_media.home_filter_news = function () {
    var arr = ['allNews', 'news', 'infographic'];
    var filters = document.querySelectorAll('.main-title__side span');

    var _loop = function _loop(i) {
        filters[i].addEventListener('click', function () {
            if (!filters[i].classList.contains('active')) {
                document.querySelector('.main-title__side span.active').classList.remove('active');
                filters[i].classList.add('active');
                action_type = 'change';
                loadmore_media.get_data("pageName=" + arr[i] + "&limit=" + 6 + "&pageIndex=" + 1);
            }
        });
    };

    for (var i = 0; i < filters.length; i++) {
        _loop(i);
    }
};

exports.default = loadmore_media;

},{"../components/media_card":3,"./helper":7}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _functions = require("./functions");

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var matches = {},
    base_url = "https://newweb.so3ody.com/";
matches.fill_teams = function (typeOfPage, selector1, selector2, input) {
	var comps_filter = document.querySelector(selector1),
	    teams_filter = document.querySelector(selector2),
	    comps_inputs = document.querySelectorAll(input),
	    comps_input_id = void 0,
	    dom_str = void 0,
	    player_id = void 0,
	    rq_url = "";

	var _loop = function _loop(i) {
		comps_input_id = comps_inputs[i].value;
		comps_inputs[i].addEventListener("change", function () {

			comps_input_id = comps_inputs[i].value;

			////////// competitions filter

			matches.filter_comp(comps_input_id, ".round-box", false);

			if (comps_input_id != "0") {
				teams_filter.classList.add("disabled");
				if (typeOfPage == "matches") {}
				if (typeOfPage == "matches") {
					matches.filter_reset(".match-card-wide", true);
					rq_url = base_url + "api/get/competition/teams?competitionId=" + comps_input_id;
				}
				if (typeOfPage == "team") {
					rq_url = base_url + "api/teams/get/comp/seasons/" + comps_input_id;
				}
				if (typeOfPage == "player") {
					player_id = comps_inputs[i].getAttribute('player-id');
					// rq_url = `${base_url}api/player/get/comp/seasons/${comps_input_id}`
					rq_url = base_url + "api/player/get_seasons?compId=" + comps_input_id + "&playerId=" + player_id;
				}
				fetch(rq_url).then(function (response) {
					return response.json();
				}).then(function (res) {
					// console.log(res)
					dom_str = "\n\t\t\t\t\t<div class=\"select-theme__current\" tabindex=\"2\">\n\t\t\t\t\t<div class=\"select-theme__value\">\n\t\t\t\t\t  <input class=\"select-theme__input\" id=\"team0\" type=\"radio\" value=\"0\" name=\"teams\" checked=\"checked\">\n\t\t\t\t\t  <p class=\"select-theme__input-text\"> " + (typeOfPage == "matches" ? "كل الفرق" : "اختر موسم") + " </p></div>\n\n\t\t\t\t\t";
					if (typeOfPage == "matches") {
						for (var _i = 0; _i < res.data.teams.length; _i++) {

							dom_str += "\n\t\t\t\t\t\t\t<div class=\"select-theme__value\">\n\t\t\t\t\t\t  <input class=\"select-theme__input\" id=\"team" + res.data.teams[_i].teamId + "\" type=\"radio\" value=\"" + res.data.teams[_i].teamId + "\" name=\"teams\" \">\n\t\t\t\t\t\t  <p class=\"select-theme__input-text\">" + res.data.teams[_i].name + "</p></div>\n\t\t\t\t\t\t\t";
						}
					}
					if (typeOfPage == "team" || "player") {
						for (var _i2 = 0; _i2 < res.data.length; _i2++) {

							dom_str += "\n\t\t\t\t\t\t\t<div class=\"select-theme__value\">\n\t\t\t\t\t\t  <input class=\"select-theme__input\" id=\"team" + res.data[_i2].seasonId + "\" type=\"radio\" value=\"" + res.data[_i2].seasonId + "\" name=\"teams\"  data-url=\"" + window.location.href + "?seasonId=" + res.data[_i2].seasonId + "\"\">\n\t\t\t\t\t\t  <p class=\"select-theme__input-text\">" + res.data[_i2].year + "</p></div>\n\t\t\t\t\t\t\t";
						}
					}

					dom_str += "<span class=\"icon select-theme__icon\">\n\t\t\t\t\t  <svg id=\"Capa_1\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 185.344 185.344\" style=\"enable-background:new 0 0 185.344 185.344;\" xml:space=\"preserve\">\n\t\t\t\t\t\t<g>\n\t\t\t\t\t\t  <g></g>\n\t\t\t\t\t\t  <path d=\"M92.672,144.373c-2.752,0-5.493-1.044-7.593-3.138L3.145,59.301c-4.194-4.199-4.194-10.992,0-15.18                                            c4.194-4.199,10.987-4.199,15.18,0l74.347,74.341l74.347-74.341c4.194-4.199,10.987-4.199,15.18,0                                            c4.194,4.194,4.194,10.981,0,15.18l-81.939,81.934C98.166,143.329,95.419,144.373,92.672,144.373z\"></path>\n\t\t\t\t\t\t</g>\n\t\t\t\t\t  </svg></span>\n\t\t\t\t  </div>\n\t\t\t\t  <ul class=\"select-theme__list\">\n\t\t\t\t\t<li>\n\t\t\t\t\t  <label class=\"select-box__option\" for=\"team0\" aria-hidden=\"aria-hidden\">" + (typeOfPage == "matches" ? "كل الفرق" : "اختر موسم") + " </label>\n\t\t\t\t\t</li>";

					if (typeOfPage == "matches") {
						for (var _i3 = 0; _i3 < res.data.teams.length; _i3++) {

							dom_str += "\n\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t  <label class=\"select-box__option\" for=\"team" + res.data.teams[_i3].teamId + "\" aria-hidden=\"aria-hidden\">" + res.data.teams[_i3].name + "</label>\n\t\t\t\t\t\t</li>";
						}
					}
					if (typeOfPage == "team" || "player") {
						for (var _i4 = 0; _i4 < res.data.length; _i4++) {

							dom_str += "\n\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t  <label class=\"select-box__option\" for=\"team" + res.data[_i4].seasonId + "\" aria-hidden=\"aria-hidden\">" + res.data[_i4].year + "</label>\n\t\t\t\t\t\t</li>";
						}
					}

					dom_str += "</ul>";
					teams_filter.innerHTML = dom_str;
					teams_filter.classList.remove("disabled");

					typeOfPage == "matches" ? matches.filter_team() : _functions2.default.select_to_redirect(".select-season input");
				});
			} else {

				teams_filter.innerHTML = "<div class=\"select-theme__current\" tabindex=\"2\">\n\t\t\t\t\t<div class=\"select-theme__value\">\n\t\t\t\t\t  <input class=\"select-theme__input\" id=\"team0\" type=\"radio\" value=\"0\" name=\"teams\" checked=\"checked\">\n\t\t\t\t\t  <p class=\"select-theme__input-text\">" + (typeOfPage == "matches" ? "كل الفرق" : "اختر موسم") + "</p>\n\t\t\t\t\t</div><span class=\"icon select-theme__icon\">\n\t\t\t\t\t  <svg id=\"Capa_1\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 185.344 185.344\" style=\"enable-background:new 0 0 185.344 185.344;\" xml:space=\"preserve\">\n\t\t\t\t\t\t<g>\n\t\t\t\t\t\t  <g></g>\n\t\t\t\t\t\t  <path d=\"M92.672,144.373c-2.752,0-5.493-1.044-7.593-3.138L3.145,59.301c-4.194-4.199-4.194-10.992,0-15.18                                            c4.194-4.199,10.987-4.199,15.18,0l74.347,74.341l74.347-74.341c4.194-4.199,10.987-4.199,15.18,0                                            c4.194,4.194,4.194,10.981,0,15.18l-81.939,81.934C98.166,143.329,95.419,144.373,92.672,144.373z\"></path>\n\t\t\t\t\t\t</g>\n\t\t\t\t\t  </svg></span>\n\t\t\t\t  </div>\n\t\t\t\t  <ul class=\"select-theme__list\">\n\t\t\t\t\t<li>\n\t\t\t\t\t  <label class=\"select-box__option\" for=\"team0\" aria-hidden=\"aria-hidden\">" + (typeOfPage == "matches" ? "كل الفرق" : "اختر موسم") + "</label>\n\t\t\t\t\t</li>\n\t\t\t\t  </ul>";
			}
		});
	};

	for (var i = 0; i < comps_inputs.length; i++) {
		_loop(i);
	}
};
matches.filter_comp = function (comp_id, comp_holder, _flag) {
	var comp_boxes = document.querySelectorAll(comp_holder);

	if (_flag == false) {
		// document.querySelector(`.no-matches`).classList.add(`d-none`)
		for (var i = 0; i < comp_boxes.length; i++) {
			if (comp_id == "0") {

				matches.filter_reset(".round-box", true);
				matches.filter_reset(".match-card-wide", true);
			} else {
				if (comp_boxes[i].getAttribute("data-competition-id") == comp_id) {
					for (var x = 0; x < comp_boxes.length; x++) {
						comp_boxes[x].classList.add("d-none");
					}
					comp_boxes[i].classList.remove("d-none");
				}
				if (comp_boxes[i].getAttribute("data-competition-id") != comp_id) {
					// matches.filter_reset(`.round-box`,false)

					comp_boxes[i].classList.add("d-none");
					//  document.querySelector(`.no-matches`).classList.remove(`d-none`)
				}
			}
		}
	} else {

		for (var _i5 = 0; _i5 < comp_boxes.length; _i5++) {
			if (comp_boxes[_i5].getAttribute("data-teama-id") == comp_id || comp_boxes[_i5].getAttribute("data-teamb-id") == comp_id) {
				for (var _i6 = 0; _i6 < comp_boxes.length; _i6++) {
					comp_boxes[_i6].classList.add("d-none");
				}
				comp_boxes[_i5].classList.remove("d-none");
			}
			if (comp_id == "0") {
				for (var _i7 = 0; _i7 < comp_boxes.length; _i7++) {
					comp_boxes[_i7].classList.remove("d-none");
				}
			}
		}
	}
};
matches.filter_team = function () {
	var team_id_input = document.querySelectorAll(".matches-teams input");

	var _loop2 = function _loop2(i) {
		team_id_input[i].addEventListener("change", function () {
			matches.filter_comp(team_id_input[i].value, ".match-card-wide", true);
		});
	};

	for (var i = 0; i < team_id_input.length; i++) {
		_loop2(i);
	}
};
matches.filter_reset = function (e, b) {

	/////////////////////////// handle case of the selected comp not playing .. you need to hide all
	var reset_all = document.querySelectorAll(e);

	if (b == true) {
		for (var i = 0; i < reset_all.length; i++) {
			reset_all[i].classList.remove('d-none');
		}
	} else {

		for (var _i8 = 0; _i8 < reset_all.length; _i8++) {
			reset_all[_i8].classList.add('d-none');
		}
	}
};
exports.default = matches;

},{"./functions":6}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _helper = require('./helper');

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var vote = {};

var box = document.querySelector('.survey__btns-box');
var btns = document.querySelectorAll('.survey__btn');
var vote_text = document.querySelector('.survey__text');

var submitted_answers = void 0;

vote.start_vote = function () {
    if (btns.length > 0) {
        var _loop = function _loop(i) {
            btns[i].addEventListener('click', function () {
                // console.log(btns[i])
                vote.request(parseInt(vote_text.getAttribute('vote-id')), parseInt(btns[i].getAttribute('answer-id')));
            });
        };

        for (var i = 0; i < btns.length; i++) {
            _loop(i);
        }
    }
};

vote.request = function (question_id, answer_id) {
    box.innerHTML = '';
    _helper2.default.add_loader('.survey__btns-box');
    _helper2.default.post_data('addVote', { 'voteId': question_id, 'answerId': answer_id }).then(function (res) {
        return res.json();
    }).then(function (data) {
        submitted_answers = data.answers;
        box.innerHTML = '';
        var html = '';
        var _class = '';
        var __all = void 0;
        var values_arr = [];
        var _percentage = [];
        for (var i = 0; i < submitted_answers.length; i++) {
            values_arr.push(submitted_answers[i].count);
        }
        __all = values_arr.reduce(function (a, b) {
            return a + b;
        }, 0);
        for (var _i = 0; _i < submitted_answers.length; _i++) {
            _percentage.push(submitted_answers[_i].count * 100 / __all);
        }
        for (var _i2 = 0; _i2 < submitted_answers.length; _i2++) {
            if (_percentage[_i2] == Math.max.apply(Math, _percentage)) {
                _class = 'high';
            } else {
                _class = '';
            }
            html += '<div class="survey__graph ' + _class + '">\n            <div class="survey__result-line flex">\n              <div class="survey__text">' + submitted_answers[_i2].text + '</div>\n              <div class="survey__num mr-max">' + Math.round(submitted_answers[_i2].count * 100 / __all) + '%</div>\n            </div>\n            <div class="survey__prog-line"> \n              <div class="survey__prog-color" style="width:' + submitted_answers[_i2].count * 100 / __all + '%;"></div>\n            </div>\n          </div>';
        }
        box.innerHTML = html;
    });
};

exports.default = vote;

},{"./helper":7}]},{},[2]);
